export default class App {
    constructor() {
        
    }

    codeExample() {
        Rainbow.color();
    }

    styleGuideTabs() {
        const $tabItem = $('.styleguide__tabs__item');
        const $tabWrapItem = $('.styleguide__tabs__wrap__item');

        const reset = () => {
            $tabItem.removeClass('active');
            $tabWrapItem.removeClass('active');
        };

        const clickEvent = (e) => {
            const $target = $(e.currentTarget);
            const tabName = $target.data('tab-name');

            reset();

            $target.addClass('active');
            $(`.styleguide__tabs__wrap__item[data-tab-name="${tabName}"]`).addClass('active');
        };

        $tabItem.click(clickEvent)
    }
    
    init() {
        this.styleGuideTabs();
        console.log('helo');
    }
}