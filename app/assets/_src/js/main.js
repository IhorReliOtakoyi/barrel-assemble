import detectBrowser from './utils/detectBrowser';
import isMobile from './utils/isMobile';
import removeHoverOnMobile from './utils/removeHoverOnMobile';
import App from "./modules/app";

$(document).ready(() => {
  detectBrowser();
  removeHoverOnMobile();

  const app = new App();

  app.init();

  if (isMobile()) {
    $('body').addClass('mobile');
  }
});


